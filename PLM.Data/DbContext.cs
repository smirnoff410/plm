﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PLM.Data.Models;

namespace PLM.Data
{
    public class DbEntities : DbContext
    {
        public DbEntities() : base("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename='|DataDirectory|Db.mdf';Integrated Security=True")
        { }
        public DbSet<BidData> Bids { get; set; }
        public DbSet<LetterData> Letters { get; set; }
        public DbSet<DriverData> Drivers { get; set; }
    }
}
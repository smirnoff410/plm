﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PLM.Data.Models;

namespace PLM.Data.Service.Write.Supplier
{
    public partial class SupplierWriteService
    {
        public void DeleteBidById(int id)
        {
            var bidData = db.Bids.FirstOrDefault(d => d.Id == id);

            if (bidData == null)
            {
                throw new Exception("Не существует запись с данным id");
            }

            db.Bids.Remove(bidData);
            db.SaveChanges();
        }

        public BidData AddBid(BidData bidData)
        {
            db.Bids.Add(bidData);
            db.SaveChanges();
            return bidData;
        }
    }
}
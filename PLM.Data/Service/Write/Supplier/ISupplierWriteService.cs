﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PLM.Data.Models;

namespace PLM.Data.Service.Write.Supplier
{
    public interface ISupplierWriteService
    {
        BidData AddBid(BidData bidData);
        void DeleteBidById(int id);

        DriverData AddDriver(DriverData driverData);
        void DeleteDriverById(int id);

        LetterData AddLetter(LetterData letterData);
        void DeleteLetterById(int id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PLM.Data.Models;

namespace PLM.Data.Service.Write.Supplier
{
    public partial class SupplierWriteService
    {
        public LetterData AddLetter(LetterData letterData)
        {
            db.Letters.Add(letterData);
            db.SaveChanges();
            return letterData;
        }

        public void DeleteLetterById(int id)
        {
            var letterData = db.Letters.FirstOrDefault(d => d.Id == id);

            if (letterData == null)
            {
                throw new Exception("Не существует запись с данным id");
            }

            db.Letters.Remove(letterData);
            db.SaveChanges();
        }
    }
}
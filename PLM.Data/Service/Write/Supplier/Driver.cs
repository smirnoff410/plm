﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PLM.Data.Models;

namespace PLM.Data.Service.Write.Supplier
{
    public partial class SupplierWriteService
    {
        public DriverData AddDriver(DriverData driverData)
        {
            db.Drivers.Add(driverData);
            db.SaveChanges();
            return driverData;
        }

        public void DeleteDriverById(int id)
        {
            var driverData = db.Drivers.FirstOrDefault(d => d.Id == id);

            if (driverData == null)
            {
                throw new Exception("Не существует запись с данным id");
            }

            db.Drivers.Remove(driverData);
            db.SaveChanges();
        }
    }
}
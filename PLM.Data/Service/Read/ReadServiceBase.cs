﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLM.Data.Service.Read
{
    public class ReadServiceBase
    {
        public DbEntities db = new DbEntities();
        public IEnumerable<T> ExecuteQuery<T>(string sql)
        {
            /*Запрос к Базе данных*/
            return db.Database.SqlQuery<T>(sql, null);
        }
    }
}

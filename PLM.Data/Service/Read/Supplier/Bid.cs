﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PLM.Data.Models;

namespace PLM.Data.Service.Read.Supplier
{
    public partial class SupplierReadService
    {
        public List<BidData> GetBidsList()
        {
            /*
             Формирование запроса к бд через ReadServiceBase
             */
            DbEntities db = new DbEntities();
            var data = db.Bids.ToList();
            return data;
        }
    }
}
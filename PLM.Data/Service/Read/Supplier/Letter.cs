﻿using PLM.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PLM.Data.Service.Read.Supplier
{
    public partial class SupplierReadService
    {
        public List<LetterData> GetLettersList()
        {
            /*
             Формирование запроса к бд через ReadServiceBase
             */
            return db.Letters.ToList();
        }
    }
}
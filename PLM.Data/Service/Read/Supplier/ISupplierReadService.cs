﻿using PLM.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLM.Data.Service.Read.Supplier
{
    public interface ISupplierReadService
    {
        List<BidData> GetBidsList();

        List<LetterData> GetLettersList();

        List<DriverData> GetDriversList();
    }
}

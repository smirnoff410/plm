﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PLM.Data.Models
{
    public class LetterData
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
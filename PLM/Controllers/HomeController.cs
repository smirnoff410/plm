﻿using PLM.Data;
using PLM.Data.Service.Read.Supplier;
using PLM.Data.Service.Write.Supplier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PLM.Data.Models;

namespace PLM.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var bid = new BidData { Name = "NewBid" };
            var driver = new DriverData { Name = "NewDriver" };
            var letter = new LetterData { Name = "NewLetter" };

            var writeService = new SupplierWriteService();

            bid = writeService.AddBid(bid);
            driver = writeService.AddDriver(driver);
            letter = writeService.AddLetter(letter);

            writeService.DeleteBidById(bid.Id);
            writeService.DeleteDriverById(driver.Id);
            writeService.DeleteLetterById(letter.Id);
            return View();
        }

        public ActionResult Letter()
        {
            return View();
        }

        public ActionResult Driver()
        {
            return View();
        }

        public ActionResult BidData()
        {
            return View();
        }
    }
}
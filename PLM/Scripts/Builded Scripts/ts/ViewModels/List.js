import "../../knockout-3.5.0";
import * as axios from "../../axios";
class ListItemType {
    constructor(Name, Id, Saved) {
        this.Item = { Name: Name, Id: Id };
        this.Saved = Saved;
    }
}
export default class DataList {
    constructor(Creator, ModelName) {
        this.ModelName = ModelName;
        this.Storage = ko.observableArray([]);
        this.ItemCreator = Creator;
        this.Delete = this.Delete.bind(this);
        this.Save = this.Save.bind(this);
        this.Add = this.Add.bind(this);
        this.Get = this.Get.bind(this);
    }
    Get() {
        axios.get(`/Api/${this.ModelName}/Get`)
            .then(response => {
            this.Storage(response.data.map(item => {
                return new ListItemType(item.Name, item.Id, true);
            }));
        })
            .catch(err => {
            console.error(err);
        });
    }
    Delete(Name, Index) {
        if (Name && this.Storage()[Index].Item.Id) {
            axios.post(`/Api/${this.ModelName}/Remove`, {
                Name: Name,
                Id: this.Storage()[Index].Item.Id
            })
                .then(response => {
                console.log(response);
                this.Storage.splice(Index, 1);
            })
                .catch(err => {
                console.log(err);
            });
        }
        else {
            this.Storage.splice(Index, 1);
        }
    }
    Save(Name, Index) {
        axios.post(`/Api/${this.ModelName}/Add`, {
            Name: Name
        })
            .then(response => {
            this.Storage()[Index] = new ListItemType(Name, response.data.InsertedId, true);
            console.log(this.Storage());
        })
            .catch(err => {
            console.log(err);
        });
    }
    Add() {
        this.Storage.push(this.ItemCreator(false));
        console.log(this.Storage());
    }
}
//# sourceMappingURL=List.js.map
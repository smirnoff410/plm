﻿export default class Driver {
    public Name: string;
    
    public static Creator(Saved: boolean) {
        return {
            Item: new Driver(),
            Saved: Saved
        };
    }
}
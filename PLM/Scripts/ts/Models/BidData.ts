﻿class BidData {
    public Name: string;

    public static Creator(Saved: boolean) {
        return {
            Item: new BidData(),
            Saved: Saved
        };
    }
}
interface BidDataIT {
    Name: string;
}

export default BidData;
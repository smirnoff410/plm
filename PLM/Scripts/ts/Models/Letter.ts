﻿export default class Letter {
    public Name: string;

    public static Creator(Saved: boolean) {
        return {
            Item: new Letter(),
            Saved: Saved
        };
    }
}
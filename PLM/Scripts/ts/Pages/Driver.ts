﻿import DataList from "../ViewModels/List";
import Driver from "../Models/Driver";
import "../../knockout-3.5.0";

var list = new DataList(Driver.Creator, "Driver");
list.Get();
ko.applyBindings({
    Data: list
});
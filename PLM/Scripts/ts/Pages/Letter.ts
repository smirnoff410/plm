﻿import DataList from "../ViewModels/List";
import Letter from "../Models/Letter";
import "../../knockout-3.5.0";

var list = new DataList(Letter.Creator, "Letter");
list.Get();
ko.applyBindings({
    Data: list
});
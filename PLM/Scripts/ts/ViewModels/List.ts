﻿import "../../knockout-3.5.0";
import * as axios from "../../axios";

class ListItemType {
    Item: Item;
    Saved: boolean;
    constructor(Name: string, Id: number, Saved: boolean) {
        this.Item = { Name: Name, Id: Id };
        this.Saved = Saved;
    }
}

interface Item {
    Name: string;
    Id: number;
}

export default class DataList<T> {
    Storage: KnockoutObservableArray<ListItemType>;
    ItemCreator: Function;
    ModelName: string;
    constructor(Creator: (Saved: boolean) => {
        Item: T,
        Saved: boolean
    }, ModelName: string) {
        this.ModelName = ModelName;
        this.Storage = ko.observableArray([]);
        this.ItemCreator = Creator;
        this.Delete = this.Delete.bind(this);
        this.Save = this.Save.bind(this);
        this.Add = this.Add.bind(this);
        this.Get = this.Get.bind(this);
    }

    public Get() {
        axios.get(`/Api/${this.ModelName}/Get`)
            .then(response => {
                this.Storage(
                    response.data.map(item => {
                        return new ListItemType(item.Name, item.Id, true);
                    })
                );
            })
            .catch(err => {
                console.error(err);
            });
    }

    public Delete(Name: string, Index: number) {
        if (Name && this.Storage()[Index].Item.Id) {
            axios.post(`/Api/${this.ModelName}/Remove`, {
                Name: Name,
                Id: this.Storage()[Index].Item.Id
            })
                .then(response => {
                    console.log(response);
                    this.Storage.splice(Index, 1);
                })
                .catch(err => {
                    console.log(err);
                });
        }
        else {
            this.Storage.splice(Index, 1);
        }
        
    }

    public Save(Name: string, Index: number) {
        axios.post(`/Api/${this.ModelName}/Add`, {
            Name: Name
        })
            .then(response => {
                this.Storage()[Index] = new ListItemType(Name, response.data.InsertedId, true);
                console.log(this.Storage());
            })
            .catch(err => {
                console.log(err);
            });
    }

    public Add() {
        this.Storage.push(this.ItemCreator(false));
        console.log(this.Storage());
    }
}
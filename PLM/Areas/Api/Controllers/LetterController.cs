﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PLM.Data.Models;
using PLM.Data.Service.Read.Supplier;
using PLM.Data.Service.Write.Supplier;

namespace PLM.Areas.Api.Controllers
{
    public class LetterController : Controller
    {
        private ISupplierReadService supplierReadService = new SupplierReadService();
        private ISupplierWriteService supplierWriteService = new SupplierWriteService();

        [HttpGet]
        public JsonResult Get()
        {
            List<LetterData> letters = supplierReadService.GetLettersList();
            return Json(letters, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Add(string Name)
        {
            try
            {
                LetterData letter = new LetterData { Name = Name };
                letter = supplierWriteService.AddLetter(letter);
                return Json(new { ItemInserted = true, InsertedId = letter.Id });
            }
            catch
            {
                return Json(new { ItemInserted = false });
            }
            
        }

        [HttpPost]
        public JsonResult Remove(string Name, int Id)
        {
            LetterData letter = new LetterData { Name = Name, Id= Id };
            supplierWriteService.DeleteLetterById(letter.Id);
            return Json(new { ItemDeleted = true });

        }
    }
}

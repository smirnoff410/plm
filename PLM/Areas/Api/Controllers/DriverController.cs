﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PLM.Data.Models;
using PLM.Data.Service.Read.Supplier;
using PLM.Data.Service.Write.Supplier;

namespace PLM.Areas.Api.Controllers
{
    public class DriverController : Controller
    {
        private ISupplierReadService supplierReadService = new SupplierReadService();
        private ISupplierWriteService supplierWriteService = new SupplierWriteService();

        [HttpGet]
        public JsonResult Get()
        {
            List<DriverData> letters = supplierReadService.GetDriversList();
            return Json(letters, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Add(string Name)
        {
            try
            {
                DriverData driver = new DriverData { Name = Name };
                driver = supplierWriteService.AddDriver(driver);
                return Json(new { ItemInserted = true, InsertedId = driver.Id });
            }
            catch
            {
                return Json(new { ItemInserted = false });
            }

        }

        [HttpPost]
        public JsonResult Remove(string Name, int Id)
        {
            DriverData driver = new DriverData { Name = Name, Id = Id };
            supplierWriteService.DeleteDriverById(driver.Id);
            return Json(new { ItemDeleted = true });
        }
    }
}
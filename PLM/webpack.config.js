/// <binding />
const path = require("path");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const AutoDLL = require("autodll-webpack-plugin");

const TS_ROOT = path.join(__dirname, "Scripts", "ts");
const JS_ROOT = path.join(__dirname, "Scripts", "js");

const ViewModelsConfig = {
    entry: {
        List: path.join(TS_ROOT, "ViewModels", "List.ts")
    },
    output: {
        path: path.join(JS_ROOT, "ViewModels"),
        filename: "[name].js",
        publicPath: "/"
    },
    resolve: {
        extensions: [".js", ".ts"]
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: "ts-loader"
            }
        ]
    },
    target: 'node',
    plugins: [
        new AutoDLL({

        }),
        new CleanWebpackPlugin([path.join(JS_ROOT, "ViewModels")])
    ]
};

const PagesConfig = {
    entry: {
        Letter: path.join(TS_ROOT, "Pages", "Letter.ts"),
        Driver: path.join(TS_ROOT, "Pages", "Driver.ts"),
        BidData: path.join(TS_ROOT, "Pages", "BidData.ts")
    },
    output: {
        path: path.join(JS_ROOT, "Pages"),
        filename: "[name].js",
        publicPath: "/"
    },
    resolve: {
        extensions: [".js", ".ts"]
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: "ts-loader"
            }
        ]
    },
    target: 'node',
    plugins: [
        new AutoDLL({

        }),
        new CleanWebpackPlugin([path.join(JS_ROOT, "Pages")])
    ]
};

const ModelsConfig = {
    entry: {
        Letter: path.join(TS_ROOT, "Models", "Letter.ts"),
        Driver: path.join(TS_ROOT, "Models", "Driver.ts"),
        BidData: path.join(TS_ROOT, "Models", "BidData.ts"),
    },
    output: {
        path: path.join(JS_ROOT, "Models"),
        filename: "[name].js",
        publicPath: "/"
    },
    resolve: {
        extensions: [".js", ".ts"]
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: "ts-loader"
            }
        ]
    },
    target: 'node',
    plugins: [
        new AutoDLL({

        }),
        new CleanWebpackPlugin([path.join(JS_ROOT, "Models")])
    ]
};

module.exports = [ViewModelsConfig, PagesConfig, ModelsConfig];
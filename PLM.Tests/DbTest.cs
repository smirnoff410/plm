﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PLM.Data;
using PLM.Data.Service.Read.Supplier;
using PLM.Data.Service.Write.Supplier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PLM.Data.Models;

namespace PLM.Tests
{
    [TestClass]
    public class DbTest
    {
        [TestMethod]
        public void GetBidsList()
        {
            ISupplierReadService supplierReadService = new SupplierReadService();
            List<BidData> bidDatas = supplierReadService.GetBidsList();
            foreach(BidData bidData in bidDatas)
                Console.WriteLine(bidData.Name);
        }

        [TestMethod]
        public void AddDriver()
        {
            ISupplierWriteService supplierWriteService = new SupplierWriteService();
            supplierWriteService.AddDriver(new DriverData { Id = 2, Name = "TestDriver" });
        }
    }
}
